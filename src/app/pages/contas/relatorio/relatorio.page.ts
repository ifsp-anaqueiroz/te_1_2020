import { Component, OnInit } from '@angular/core';
import { ContaService } from '../service/conta-service';

@Component({
  selector: 'relatorio',
  templateUrl: './relatorio.page.html',
  styleUrls: ['./relatorio.page.scss'],
})
export class RelatorioPage implements OnInit {
  listaContas;

  constructor(
    private conta: ContaService
  ) { }

ngOnInit() {
 this.conta.listaCompleta().subscribe(x => this.listaContas = x);
}




}
