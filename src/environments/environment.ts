// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: 'AIzaSyCWUopr3GE_HfCwDamHwv_mt4Qq3O6Dn8k',
    authDomain: 'controle-ifsp-gru.firebaseapp.com',
    databaseURL: 'https://controle-ifsp-gru.firebaseio.com',
    projectId: 'controle-ifsp-gru',
    storageBucket: 'controle-ifsp-gru.appspot.com',
    messagingSenderId: '583505937462',
    appId: '1:583505937462:web:68b87e3ba091b8ee390497',
    measurementId: 'G-PMBF35JVPK'
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
